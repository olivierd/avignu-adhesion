#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
#

'''Goal of this little script, is to parse vCard file (.vcf) and
replace block annotation from LaTeX template in order to have
complete document (.tex).
'''

import argparse
import io
import mimetypes
import os
import pathlib
import re
import sys

try:
    import vobject
except ImportError:
    print('Additional module is required')
    sys.exit(-1)


class FinalizeLaTeX(object):

    def __init__(self, vcf, tex):
        self.data = None

        # We read the whole document
        if isinstance(vcf, io.TextIOWrapper):
            self.data = vcf.read()
            vcf.close()

        if isinstance(tex, io.TextIOWrapper):
            self.out = open(self.flatx_set_new_name(tex.name), 'w')
            self.f = tex

    def flatx_set_new_name(self, name, ext='*.in'):
        p = pathlib.Path(name).resolve()

        # Search suffix
        if p.match(ext):
            n = p.stem
            return '{0}'.format(p.parent.joinpath(n).with_suffix('.tex'))
        else:
            return '{0}'.format(p.with_suffix('.tex'))

    def flatx_finalize(self):
        # Patterns
        p = re.compile(r'(.+?\{(?P<email>##EMAIL##)\}|(?P<vcard>##VCARD##))')
        line = self.f.readline()
        v = vobject.readOne(self.data)
        flag = False

        while line:
            res = p.search(line)
            if res is not None:
                if res.group('email') is not None:
                    value = self.flatx_get_email(v)
                    if value is not None:
                        flag = True

                        self.out.write(line.replace(res.group('email'),
                                                    value))
                if res.group('vcard') is not None:
                    value = self.flatx_get_personal_info(v)
                    if value:
                        if flag:
                            value.append('& \href{mailto:\contact}{\contact} \\\\')
                        value.append(os.linesep)
                        self.out.write(os.linesep.join(value))
            else:
                self.out.write(line)

            line = self.f.readline()

        self.f.close()
        self.out.close()

    def flatx_get_email(self, v):
        res = None

        if 'email' in v.contents:
            res = '{0}'.format(v.email.value)

        return res

    def flatx_get_personal_info(self, v):
        res = []

        if 'n' in v.contents:
            r = '\\textsc{%s} %s \\\\' % (v.n.value.family,
                                          v.n.value.given)
            if 'gender' in v.contents:
                if v.gender.value == 'F':
                    r = '& Mme {0}'.format(r)
                elif v.gender.value == 'M':
                    r = '& M {0}'.format(r)
                else:
                    r = '& {0}'.format(r)
            res.append(r)

        if 'adr' in v.contents:
            if v.adr.value.extended != '':
                res.append('& {0} \\\\'.format(v.adr.value.extended))

            res.append('& {0} - {1} {2} \\\\'.format(v.adr.value.street,
                                                     v.adr.value.code,
                                                     v.adr.value.city))

        return res

def check_mime_type(filename, subtype='text'):
    '''Check file with particular MIME type.'''

    res = False

    p = pathlib.Path(filename).resolve()
    if p.exists():
        file_type, file_encoding = mimetypes.guess_type(p.as_uri())
        if file_type is not None and subtype in file_type:
            res = True

    return res

def main(args):
    # vCard file object (we have one element)
    vcard_obj = args.input
    # LaTeX file object
    tex_obj = args.template

    # Check if we have a real vCard file
    if not check_mime_type(vcard_obj.name, 'vcard'):
        print('Error: not valid vCard format')
        sys.exit(-1)
    else:
        f = FinalizeLaTeX(vcard_obj, tex_obj)
        f.flatx_finalize()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True,
                        type=argparse.FileType('r'),
                        help='vCard file to parse')
    parser.add_argument('-t', '--template', required=True,
                        type=argparse.FileType('r'),
                        help='LaTeX template')
    main(parser.parse_args())
