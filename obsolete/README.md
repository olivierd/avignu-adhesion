# Formulaire d'adhésion

Ce dépôt contient le modèle pour générer le formulaire d'adhésion à
l'association [AviGNU](https://avignu.com/).

**Il n'y a pas de données sensibles.**

## Pré requis

Le formulaire est écrit en LaTeX et repose sur le moteur **XeTeX**.

Pour les utilisateurs des distributions Debian et « Ubuntu-like »,
assurez-vous d'avoir installé le paquet `texlive-xetex`.

Pour les utilisateurs de Fedora, le méta paquet `texlive` est *bugué*, vous
pouvez consulter cette [page](https://avignu.wiki.tuxfamily.org/doku.php?id=documentation:linux:fedora:xetex) pour avoir un environnement
fonctionnel. Il faudra également le paquet `texlive-multirow`.

## Comment générer le PDF

Tout d'abord il faut une fiche [vCard](https://fr.wikipedia.org/wiki/VCard)
contenant les informations du trésorier.

Des exemples sont disponibles dans le dossier [tests](./tests/).

Ensuite il faut installer le paquet `python3-vobject`.

Puis, on va générer le document LaTeX final, de cette manière :

```
python3 finalize-latex.py -i tresorier.vcard -t adhesion.tex.in
```

* L'option **-i** (ou **--input**) indique le chemin vers la fiche vCard
* L'option **-t** (ou **--template**) indique le chemin vers le *template*

Un fichier `.tex` va être généré, c'est ce dernier qui va produire le PDF.

```
xelatex adhesion.tex
```

Un `Makefile` est également disponible.

```
make
```

ou

```
make pdf
```

L'image PNG utilisée, provient de ce [dépôt](https://framagit.org/olivierd/avignu-images).
