# Formulaire d'adhésion

Ce dépôt contient le modèle pour générer le formulaire d'adhésion à
l'association [AviGNU](https://avignu.com/).

Pour générer le PDF, il suffit de taper la commande :

```
make
```

ou

```
make pdf
```

L'image PNG utilisée, provient de ce [dépôt](https://framagit.org/olivierd/avignu-images).
