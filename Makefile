# SPDX-License-Identifier: Unlicense
#
# Create PDF from .odt file. It requires LibreOffice.
#

SRC=	adhesion.odt

LO_CMD:=	/usr/bin/soffice
LO_ARGS=	--headless --convert-to


all: pdf

# Available filters,
# https://cgit.freedesktop.org/libreoffice/core/tree/filter/source/config/fragments/filters/
pdf:
	$(LO_CMD) $(LO_ARGS) pdf:"writer_pdf_Export" \
		--outdir $(CURDIR) $(SRC)

clean:
	@$(RM) $(subst .odt,.pdf,$(SRC))
